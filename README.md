## JMCase node.js server on GraphQL with a prisma database

How to run it:

1. Open terminal and cd to directory my-app within the project directory
2. Run: npm install -g prisma
3. Run: npm install
3. If you make changes to the code: Run: prisma deploy
4. Run: yarn dev

---

